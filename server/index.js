const express = require("express");
const cors = require("cors");
const axios = require("axios").default;
const app = express();
const port = 8000;
const { DateCars, filterCars } = require("./cars");

app.use(express.json());
app.use(cors());


app.get("/", (req, res) => {
    res.send({
      message: "Success",
    });
});

app.post("/api/cars", async (req, res) => {
    const cars = (await axios.get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json")).data;
    const newCars = await DateCars(cars);
    const filteredCars = await filterCars(newCars, req.body);
    res.send(filteredCars);
});


app.listen(port, () => {
    console.log(`Server is Running at http://localhost:${port}`);
});
  